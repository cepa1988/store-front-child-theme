# Storefront Child Theme

The StoreFront Child Theme is a starter blank child theme for WooThemes StoreFront WooCommerce theme.

## Installation

1. Download the child theme from it's GitHub Repository [Download StoreFront Child Theme](s://cepa1988@bitbucket.org/cepa1988/store-front-child-theme.git).
2. Goto WordPress > Appearance > Themes > Add New.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

## Usage

This child theme is designed to be used as a starter theme for the WooCommerce StoreFront theme which you can download for free below.

* [Download WooCommerce StoreFront Theme](https://wordpress.org/themes/storefront/)
* [StoreFront Documentation](http://docs.woocommerce.com/documentation/themes/storefront/)
* [StoreFront Child Themes](https://woocommerce.com/product-category/themes/storefront-child-theme-themes/)
* [StoreFront Extensions](https://woocommerce.com/product-category/storefront-extensions/)

Custom PHP that you write should be added to the child themes functions.php file whilst any custom CSS should be added to the child themes style.css file.
