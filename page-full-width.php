<?php
/*
Template Name: About Page
Template Post Type: page
*/
$f = get_fields();
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="section-wrapper">
  <!-- heading-section -->
  <div class="heading-section">
    <div class="image">
      <?php echo wp_get_attachment_image( $f['heading_image'], full ); ?>
    </div>
    <div class="content">
      <?=$f['heading_description']?>
      <?php if( $f['heading_link'] ): ?>
      <a href="<?= $f['heading_link']['url']?>"><?= $f['heading_link']['title']?></a>
      <?php endif ?>
    </div>
  </div>
  <!-- heading-section -->
  <!-- Featured Content -->
  <div class="featured-section">
    <div class="title">
        <small><?= $f['featured_sub_title']?></small>
        <h2><?= $f['featured_tilte']?></h2>
    </div>
    <div class="content">
        <?= $f['featured_content']?>
    </div>
  </div>
  <!-- Featured Content end-->
  <!-- Block Section -->
  <div class="block-section">
    <?php 
$rows = $f['block_repeater'];
?>
    <ul class="parent">
      <?php
      $itmes = $f['block_repeater'];
      if($itmes)
          foreach($itmes as $item)
          { ?>
          <li>
            <?= wp_get_attachment_image($item['image'])?>
            <small><?= $item['tag_line']?></small>
            <h3><?= $item['title']?></h3>
          </li>
      <?php }?>
    </ul>
  </div>
  <!-- Block Section end-->
</div>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
